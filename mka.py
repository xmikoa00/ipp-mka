#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

__author__= "Albert Miko"
__email__ = "berci1992_at_gmail_com"

"""
IPP project task 2: Minimization of Finite Automata

The script reads a description of well specified finite automaton from input
and outputs a minimized equivalent.
The algorithm used can be found at: 
[1]  https://www.fit.vutbr.cz/study/courses/IFJ/private/prednesy/Ifj04-cz.pdf
"""

import sys
import argparse   # parse cmd arguments
import string
import re         # regular expressions

class FA_SyntaxError(Exception):
    """
    Raised when syntax error in the input
    """
    pass

class FA_SemanticError(Exception):
    """
    Raised when semantic error in the input
    
    semantic errors are:
     - input alphabet is empty
     - rule uses symbol or state not in set of symbols/states
     - starting state not in set of states
     - set of final states not subset of states
    """
    pass

class FA_NotWellSpecified(Exception):
    """
    Raised when FA is not a well specified FA
    """
    pass

class FA_NotDeterministic(Exception):
    """
    Raised when FA is not deterministic (used with --wsfa option)
    """
    pass

class FA_Rule:
    """
    Represents a single rule in a finite automaton
    """
    def __init__(self,fa,this,symbol,next):
        """
        set attributes, do semantic check
        @raises FA_SemanticError
        @param fa:     automaton the rule belongs to
        @param this:   state of automaton before application of rule
        @param next:   state of automaton after  application of rule
        @param symbol: symbol of input alphabet
        """
        if (this not in fa.states):
            raise FA_SemanticError()
        if (next not in fa.states):
            raise FA_SemanticError()
        if (symbol not in fa.symbols and symbol!=""):
            raise FA_SemanticError()
        self.stThis = this
        self.stNext = next
        self.input  = symbol

    def __repr__(self):
        if self.input=="'": input="''"
        else :              input = self.input
        repr = self.stThis + " '" + input + "' -> " + self.stNext
        return repr


class FiniteAutomaton:
    """
    Internal representation of finite automaton

    symbols and states are represented by strings
    rules are represented by FA_Rule objects
    """
    def __init__(self):
        self.symbols = [] 
        self.states  = []
        self.start_state = ""
        self.finish_states = []
        self.rules = [] # list of FA_Rule objects

    def lower(self):
        return self.__repr__().lower()

    def __repr__(self):
        """
        return string representation on FA in normal form
        """
        repr = "(\n{"
        if (self.states != []):
            self.states.sort()
            repr += self.states[0]
            for state in self.states[1:]:
                repr = repr + ", " + state
        repr += "},\n{"
        if (self.symbols != []):
            self.symbols.sort()
            if self.symbols[0]=="'": repr += "''''"
            else: repr += "'" + self.symbols[0] + "'"
            for symbol in self.symbols[1:]:
                if symbol=="'": symbol = "''"
                repr = repr + ", '" + symbol + "'"
        repr += "},\n{\n"
        if (self.rules != []):
            self.sort_rules()
            repr += self.rules[0].__repr__()
            for rule in self.rules[1:]:
                repr = repr + ",\n" + rule.__repr__()
        repr = repr + "\n},\n" + self.start_state + ",\n{"
        if (self.finish_states != []):
            self.finish_states.sort()
            repr += self.finish_states[0]
            for state in self.finish_states[1:]:
                repr = repr + ", " + state
        repr += "}\n)"
        return repr

    def add_state(self,state):
        """
        add new state to set of states 
        """
        if state not in self.states:
            self.states.append(state)

    def add_symbol(self,symbol):
        """
        add new symbol to input alphabet
        """
        if symbol not in self.symbols:
            self.symbols.append(symbol)

    def add_rule(self,rule):
        """
        add new rule to set of rules
        note: semantic check of rule is done in the FA_Rule constructor
        """
        for r in self.rules:
            if (rule.stThis==r.stThis and rule.input==r.input and 
                rule.stNext==r.stNext): return
        self.rules.append(rule)
 
    def set_start_state(self,state):
        """
        set new start state
        overwrites old start state
        @raises FA_SemanticError if state is not in set of states
        """
        if state not in self.states:
            raise FA_SemanticError
        self.start_state = state

    def add_fstate(self,state):
        """
        add new final state to set of final states
        @raises FA_SemanticError if state is not in set of states
        """
        if state not in self.states:
            raise FA_SemanticError
        self.finish_states.append(state)

    def sort_rules(self):
        """
        sort the rules of FA based on 3 keys(listed from highest 
        to lowest priority):
         - initial state    
         - input symbol
         - resulting state
        """
        self.rules.sort(key=lambda FA_Rule: FA_Rule.stNext)
        self.rules.sort(key=lambda FA_Rule: FA_Rule.input)
        self.rules.sort(key=lambda FA_Rule: FA_Rule.stThis)

    def find_unreachable(self):
        """
        Finds unreachable states of deterministic FA
        @return: list of unreachable states
        """
        rule_st0s = []
        rule_st1s = []
        for rule in self.rules:
            rule_st0s.append(rule.stThis)
            rule_st1s.append(rule.stNext)
        st_reachable = [self.start_state]
        st_unchecked = self.states[:]
        st_unchecked.remove(self.start_state)
        prev_len = len(st_unchecked)+1
        while len(st_unchecked)!=prev_len:
            prev_len = len(st_unchecked)
            for i in range(len(rule_st0s)):
                if rule_st0s[i] in st_reachable:
                    st_reachable.append(rule_st1s[i])
                    if rule_st1s[i] in st_unchecked:
                        st_unchecked.remove(rule_st1s[i])
        return st_unchecked

    def check_deterministic(self):
        """
        Checks if FA is deterministic:
         - no epsilon rules
         - there is at most one rule for every state-input combination
        @raises FA_NotDeterministic
        """
        rule_st0s = []
        rule_inps = []
        for rule in self.rules:
            if rule.input=="": raise FA_NotDeterministic
            rule_st0s.append(rule.stThis)
            rule_inps.append(rule.input)
        for i in range(len(rule_st0s)):
            for j in range(len(rule_st0s)):
                if i==j: continue
                if rule_st0s[i]==rule_st0s[j] and rule_inps[i]==rule_inps[j]:
                    raise FA_NotDeterministic

    def check_well_specified(self):
        """
        Checks if FA is well specified
        @raises FA_NotWellSpecified
        """
        try:
            self.check_deterministic()
        except FA_NotDeterministic:    
            raise FA_NotWellSpecified
        # check unreachable states
        st_unreachable = self.find_unreachable()
        if len(st_unreachable)!=0:
            raise FA_NotWellSpecified    
        #check if every state has rule for every symbol
        for st in self.states:
            symlist = []
            for rule in self.rules:
                if rule.stThis==st:
                    if rule.input in symlist: raise FA_NotWellSpecified
                    symlist.append(rule.input)
            for sym in self.symbols:
                if sym not in symlist: raise FA_NotWellSpecified 
        # check if has at most 1 non-finishing state        
        non_finishing =  self.find_non_finishing()
        if len(non_finishing)>1: # has at least one non-finishing state
            raise FA_NotWellSpecified

    def wsfa(self):
        """
        Transforms deterministic FA to well specified FA
        """
        # get states to remove(non-finishing and unreachable)
        self.check_deterministic()
        rst = self.find_non_finishing()
        rst.extend(self.find_unreachable())
        self.add_state("qFALSE")
        for rule in self.rules: 
            # remove rules to non-finishing and unreachablestates
            if rule.stThis in rst or rule.stNext in rst:
                self.rules.remove(rule)
        for st in rst: # remove non-finishing states
            if st in self.states: self.states.remove(st)
            if st in self.finish_states: self.finish_states.remove(st)
            if st == self.start_state: self.set_start_state("qFALSE")
        for st in self.states: # add rules to qFALSE
            st_sym = []
            for rule in self.rules: 
                if rule.stThis==st: st_sym.append(rule.input)
            for sym in self.symbols:
                if sym not in st_sym: 
                    self.add_rule(FA_Rule(self,st,sym,"qFALSE"))
        return self

    def find_non_finishing(self):
        """
        Finds all non finishing states of FA
        for well specified FA, a list of one state is returned
        @return: list of names of non-finishing states (or empty list)
        """
        rule_st0s = []
        rule_st1s = []
        for rule in self.rules:
            rule_st0s.append(rule.stThis)
            rule_st1s.append(rule.stNext)
        st_unchecked = self.states[:]
        st_finishing = self.finish_states[:]
        for st in st_finishing: 
            st_unchecked.remove(st)
        prev_len = len(st_finishing)-1
        while len(st_finishing)!=prev_len:
            prev_len = len(st_finishing)
            for i in range(len(rule_st0s)):
                if rule_st1s[i] in st_finishing:
                    if rule_st0s[i] not in st_finishing:
                        st_finishing.append(rule_st0s[i])
                    if rule_st0s[i] in st_unchecked:
                        st_unchecked.remove(rule_st0s[i])
        return st_unchecked

    def minimize(self):
        """
        Minimizes well specified FA
        """
        def join_states(state_list):
            if len(state_list)==0: return "_"
            jstate = state_list[0]
            for state in state_list[1:]: jstate = jstate + "_" + state
            return jstate
        def disjunct(L1,L2,stX): # check if L1+L2==stX and L1*L2==EMPTY
            for i in L1:
              for j in L2:
                if i==j: return False
            stX_copy = stX[:]
            for i in L1: stX_copy.remove(i)
            for i in L2: 
              if i in stX_copy: stX_copy.remove(i)
            if len(stX_copy)==0: return True
            return False 
        def divide(states_list,self):
            """divide a state in state_list"""
            for d in self.symbols:
              for stX in states_list:
                if len(stX)==1: continue
                rule_st0s = []
                rule_st1s = []
                for rule in self.rules:
                  if rule.input==d and rule.stThis in stX:
                    rule_st0s.append(rule.stThis)
                    rule_st1s.append(rule.stNext)
                bases = []
                for stY in states_list:
                  baseY = []
                  for i in range(len(rule_st0s)):
                    if rule_st1s[i] in stY: baseY.append(rule_st0s[i])
                  if baseY!=[]:bases.append(baseY)
                for b in bases:
                  if b==[]: bases.remove(b) 
                br = False
                for L1 in bases:
                  for L2 in bases:
                    if L1==L2: continue
                    if not disjunct(L1,L2,stX): continue
                    states_list.remove(stX)
                    states_list.append(L1)
                    states_list.append(L2)
                    br=True;break
                  if br: break
                if br: break
            for st_list in states_list:
                st_list.sort()
            return states_list
        minfa = FiniteAutomaton()
        minfa.symbols = self.symbols
        non_finishing = self.find_non_finishing()
        non_finals = self.states[:]
        for state in non_finishing: non_finals.remove(state)
        for state in self.finish_states:
            if state in non_finals: non_finals.remove(state)
        states_list = [self.finish_states[:],non_finals]
        if len(non_finishing)>0: states_list.append(non_finishing)
        prev_len = len(states_list)-1
        while prev_len<len(states_list):       
            prev_len = len(states_list)
            states_list = divide(states_list,self)
        for st_list in states_list: # add new states
            minfa.add_state(join_states(st_list)) 
        for stA in states_list: # add new rules
            for stB in states_list:
                for rule in self.rules:
                    if rule.stThis in stA and rule.stNext in stB:
                        minfa.add_rule(FA_Rule(minfa,join_states(stA),
                                       rule.input,join_states(stB))) 
        for st_list in states_list: # set new start state
            if self.start_state in st_list: 
                minfa.set_start_state(join_states(st_list))
        for st_list in states_list: # add new finishing states
            for st in st_list:
                if st in self.finish_states:
                     minfa.add_fstate(join_states(st_list))
                     break  
        self = minfa
        return self

    def analyse_string(self,str):
        """
        Checks if string is in language of FA
        @param str: string to analyse
        @return: 1|0 (True|False)
        """
        stThis = self.start_state
        for c in str: 
            if c not in self.symbols: 
                return 0
            for rule in self.rules:
                if rule.input==c and rule.stThis==stThis:
                    stThis = rule.stNext
                    break
        if stThis in self.finish_states: 
            return 1
        return 0

class Tokenize():
    """
    Implements lexical analyzer

    The instantiated object holds information about the input string
    and the last token read(type and value).
    Token types are defined as class variables(constants): 
      EMPTY, BR_L, BR_R, PAR_L, PAR_R, COMMA, DOT, ARROW, SYM, STATE
    All get_*() methods update self.index
    self.type and self.value are updated as stated in docstrings 
      of individual methods
    """
    EMPTY =  0    # undefined token 
    BR_L  = 10    # left  brace '{'
    BR_R  = 11    # right brace '}'
    PAR_L = 20    # left  parentheses '('
    PAR_R = 21    # right parentheses ')'
    COMMA = 30    # ','
    DOT   = 31    # '.'
    ARROW = 40    # '->'
    SYM   = 50    # symbol of input alphabet
    STATE = 60    # state of finite automaton

    def __init__(self,input_str):
        """
        @param input_str: FA from input file read into a string
        self.type and self.value hold information about the last token read
        self.index is index into self.inp_str string
        """
        self.type    = self.__class__.EMPTY
        self.value   = None
        self.inp_str = input_str
        self.index   = 0
    
    def skip_comments(self):
        """
        Skip all insignificant whitespace and comments
        Set self.index to next meaningful character in self.inp_str
        """
        if self.index==len(self.inp_str): return
        while self.inp_str[self.index] in string.whitespace:
            self.index += 1
            if self.index==len(self.inp_str): return
        if self.inp_str[self.index]=='#':
            while self.inp_str[self.index]!='\n': 
                self.index += 1
                if self.index==len(self.inp_str): return
            self.skip_comments()    

    def get_interpunc(self):
        """
        Try to read next token, should be BR_L,BR_R,PAR_L,PAR_R,COMMA or DOT
        updates self.type
        @raise FA_SyntaxError: if next token is none of listed above
        """
        self.skip_comments()
        c = self.inp_str[self.index]
        if   c=='{': self.type = self.__class__.BR_L
        elif c=='}': self.type = self.__class__.BR_R
        elif c=='(': self.type = self.__class__.PAR_L
        elif c==')': self.type = self.__class__.PAR_R
        elif c=='.': self.type = self.__class__.DOT
        elif c==',': self.type = self.__class__.COMMA
        else       : raise FA_SyntaxError
        self.index += 1
        return self

    def get_arrow(self):
        """
        Try to read next token, should be ARROW ("->")
        updates self.type 
        @raise FA_SyntaxError: if next token is not ARROW
        """
        self.skip_comments()
        if self.inp_str[self.index]!='-':
            raise FA_SyntaxError
        self.index += 1
        if self.inp_str[self.index]!='>':
            raise FA_SyntaxError
        self.index += 1
        self.type = self.__class__.ARROW  
        return self

    def get_state(self):
        """
        Try to read next token, should be STATE
        updates self.type and self.value
        @raise FA_SyntaxError: if next token is not STATE
        """
        self.skip_comments()
        state_regex="[a-zA-Z][a-zA-Z0-9_]*"
        state_match = re.match(state_regex,self.inp_str[self.index:])
        if state_match is None or state_match.group(0)[-1]=='_':
            raise FA_SyntaxError
        self.value = state_match.group(0)
        self.index += len(self.value)
        self.type = self.__class__.STATE
        return self

    def get_symbol(self):
        """
        Try to read next token, should be SYM
        updates self.type and self.value
        @raise FA_SyntaxError: if next token is not SYM
        """
        self.skip_comments()
        sym_regex=re.compile("('''')|('')|('[^']')|([^ '.,#->(){}])|[0-9]",re.S|re.M)
        sym_match = sym_regex.match(self.inp_str[self.index:]) 
        if sym_match is None:
            raise FA_SyntaxError
        m = sym_match.group(0)
        self.index += len(m)
        if   m=="''''":
            self.value = "'"
        elif m=="''":
            self.value = ""
        elif m[0]=="'":
            self.value = m[1:-1]
        else:
            self.value = m
        self.type = self.__class__.SYM
        return self

def parse_args():
    """
    parse command line arguments
    @return: argparse.Namespace object with argument values
    """
    my_argparser = argparse.ArgumentParser(
        description='IPP project: MKA in python3',
        add_help=True
        )
    choose_mf = my_argparser.add_mutually_exclusive_group() 
    choose_mf.add_argument('-f','--find-non-finishing',
        action='store_true',
        help='find non finishing state of well specified FA')
    choose_mf.add_argument('-m','--minimize',
        action='store_true',
        help='minimize well specified FA from input')
    choose_mf.add_argument('--analyse-string',
        help='check if string is valid in language of FA')
    my_argparser.add_argument('-i','--case-insensitive',
        action='store_true',
        help="case in input will be ignored, output will be lowercase")
    my_argparser.add_argument('-r','--rules-only',
        action='store_true',
        help='input only rules for FA')
    my_argparser.add_argument('--wsfa',
        action='store_true',
        help='accept any deterministic FA, transform it to well specified FA')
    my_argparser.add_argument('--input',
        help='specify input file, default is STDIN',
        type=argparse.FileType('r'),default='-')
    my_argparser.add_argument('--output',
        help='specify output file, default is STDOUT',  
        type=argparse.FileType('w'),default='-')
    return  my_argparser.parse_args()

def parse_rules(fa,tok):
    """
    Implement input parsing for extension RLO
    """
    def get_rule(fa,tok,start=False):
        st0 = tok.get_state().value
        fa.add_state(st0)
        if start: fa.set_start_state(st0)
        inp = tok.get_symbol().value
        fa.add_symbol(inp)
        tok.get_arrow()
        st1 = tok.get_state().value
        fa.add_state(st1)
        if (tok.inp_str[tok.index]=='.'):
            fa.add_fstate(st1)
            tok.get_interpunc()
        fa.add_rule(FA_Rule(fa,st0,inp,st1))
        tok.skip_comments()
    get_rule(fa,tok,True)
    while tok.index!=len(tok.inp_str):
        if tok.get_interpunc().type!=Tokenize.COMMA:
            raise FA_SyntaxError
        get_rule(fa,tok)
    return fa

def parse_input(inp_str, rules_only):
    """
    parse input file
    @param inp_str: content of input file
    @type  inp_str: unicode string
    @param rules_only: value of -r command line switch
    @type  rules_only: boolean
    @return: finite automaton in internal representation 
    """
    fa = FiniteAutomaton()
    tok = Tokenize(inp_str)
    
    if rules_only: return parse_rules(fa,tok)

    tok.get_interpunc()             # opening parentheses
    if tok.type!=Tokenize.PAR_L:
        raise FA_SyntaxError
    tok.get_interpunc()             # opening braces of STATES section
    if tok.type!=Tokenize.BR_L:
        raise FA_SyntaxError
    
    def get_group(func1,func2):
        """
        get values out of groups enclosed in braces
        @param func1: FiniteAutomaton.{add_symbol|add_state|add_fstate}
        @param func2: Tokenize.{get_symbol|get_state}
        """
        while True:                     
            func1(func2().value)
            tok.get_interpunc()         # comma or closing braces
            if tok.type==Tokenize.BR_R: 
                return
            if tok.type!=Tokenize.COMMA:
                raise FA_SyntaxError

    def next_group():
        tok.get_interpunc()             # group closed, get comma
        if tok.type!=Tokenize.COMMA:
            raise FA_SyntaxError
        tok.get_interpunc()             # opening braces of next section
        if tok.type!=Tokenize.BR_L:
            raise FA_SyntaxError

    get_group(fa.add_state,tok.get_state) # STATES of FA

    next_group()                    # SYMBOLS of input alphabet
    get_group(fa.add_symbol,tok.get_symbol)

    next_group()                    # RULES
    while True:
        rule_st0 = tok.get_state().value
        rule_sym = tok.get_symbol().value
        tok.get_arrow()
        rule_st1 = tok.get_state().value
        fa.add_rule(FA_Rule(fa,rule_st0,rule_sym,rule_st1))
        tok.get_interpunc()
        if tok.type==Tokenize.BR_R:
            break
        if tok.type!=Tokenize.COMMA:
            raise FA_SyntaxError
    
    if tok.get_interpunc().type != Tokenize.COMMA:
        raise FA_SyntaxError
    fa.set_start_state(tok.get_state().value) # START_STATE
    
    next_group()                    # FSTATES
    get_group(fa.add_fstate,tok.get_state)

    if tok.get_interpunc().type!=Tokenize.PAR_R: # closing parentheses
        raise FA_SyntaxError

    if fa.symbols == [] or fa.states == []:
        raise FA_SemanticError

    tok.skip_comments()
    if tok.index!=len(tok.inp_str):
        raise FA_SyntaxError 

    return fa 

class CatchStream:
    """
    redirects stream into a string
    Reference: www.stackoverflow.com/questions/4347983/
    """
    def __init__(self):    self.value = ""
    def __repr__(self):    return self.value
    def write(self, text): self.value += text

if __name__=="__main__":

    tmpStderr = sys.stderr       # save original stderr
    catchStderr = CatchStream()
    sys.stderr = catchStderr     # redirect stderr into string for analysis

    try:                         # patse cmd arguments
        args = parse_args()
    except SystemExit:           # analyse stderr and decide exit code
        print (catchStderr,file=tmpStderr)
        if  ("--input:"  in catchStderr.value): exit(2)
        elif("--output:" in catchStderr.value): exit(3)
        else:                                   exit(1)

    sys.stdin  = args.input
    sys.stdout = args.output
    sys.stderr = tmpStderr       # restore stderr

    inp_str = args.input.read()  # read input file
    if args.case_insensitive:    # transform to lowercase if -i 
        inp_str = inp_str.lower()
     
    try: # extract finite automaton from input
        fa = parse_input(inp_str,args.rules_only) 
        fa.check_well_specified()    # check if FA is well specified
    except FA_SyntaxError:
        print("Syntax error in input file",file=sys.stderr) 
        exit(60) 
    except FA_SemanticError:
        print("Semantic error in input file",file=sys.stderr)
        exit(61)
    except FA_NotWellSpecified:
        if (args.wsfa):
            try: fa = fa.wsfa()           # transform to well specified FA
            except FA_NotDeterministic:
                print("Not deterministic finite automaton",file=sys.stderr)
                exit(62) #???
        else:
            print("Not a well specified finite automaton",file=sys.stderr)
            exit(62)
    
    if args.find_non_finishing:  # find non finishing state
        nf = fa.find_non_finishing()
        if len(nf)>0: out_str = nf[0]
        else: exit(0)
    elif args.minimize:          # minimize finite automaton
        out_str = fa.minimize()
    elif args.analyse_string is not None: # and args.analyse_string!="" :
        out_str = fa.analyse_string(args.analyse_string)
    else:
        out_str = fa 

    if(args.case_insensitive): outstr = out_str.lower()
    print(out_str,end='')
     
    exit(0) 
# EOF
