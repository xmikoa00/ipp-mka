This is a solution to an old homework assignment: minimize a well specified finite automaton.
The FA was given as a text string in a regular language, and the result had to be returned in the same format -- so there is a parser and a printer for that FA description language in the script.

The script also implements some extensions to the base assignment:
 - check if the input FA is deterministic,
 - check if the input FA is well specified,
 - simulate the actions of the FA on the provided input string.